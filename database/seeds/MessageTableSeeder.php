<?php

use App\Message;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */

    //create 50 message with random user
    public function run()
    {
        $faker = Faker::create();
        $userIds = DB::table('users')->pluck('id');

        foreach (range(1, 50) as $index) {
            Message::create([
                'body' => $faker->text,
                'user_id' => $faker->randomElement($userIds)
            ]);
        }
    }
}
