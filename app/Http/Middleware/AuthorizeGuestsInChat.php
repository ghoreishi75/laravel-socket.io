<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AuthorizeGuestsInChat
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //if user is not authenticated


        if (auth()->check()) {
            return $next($request);

        }
        if (auth()->guest()) {
            //authorize as guest
            $this->authorizeAsGuest();
        }
        return $next($request);

    }

    public function authorizeAsGuest()
    {
        auth()->login($this->guestUser());
        // request()->merge(['user' => $this->guestUser()]);
        // request()->setUserResolver(function () {
        //     return $this->guestUser();
        // });
    }

    public function guestUser()
    {
        return User::firstOrCreate(['name' => 'guest', 'email' => 'email', 'password' => 'pass']);
    }

    // public static function GetApi($url)
    // {
    //     $client = new \GuzzleHttp\Client();
    //     $request = $client->get($url);
    //     $response = $request->getBody();
    //     return $response;
    // }
}
