<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Http\Requests\ChatSendRequest;
use App\Http\Resources\MessageCollection;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{

    public function index()
    {
        $messages = Message::all()->sortByDesc('created_at');
        return MessageCollection::collection($messages);
    }

    public function view()
    {
        return view('chat.show');
    }

    public function send(ChatSendRequest $request)
    {
        $body = request('body');
        $message = Message::create(
            [
                'body' => $body,
                'user_id' => Auth::id()
            ]
        );
        event(new MessageSent(new MessageCollection($message)));

        return new MessageCollection($message);
    }
}
